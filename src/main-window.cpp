#include "main-window.h"
#include "translator.h"
#include "config.h"

#include <memory>

#include <QAction>
#include <QApplication>
#include <QDebug>
#include <QFileDialog>
#include <QSqlDatabase>
#include <QSqlQuery>

#include <KActionCollection>
#include <KLocalizedString>
#include <KStandardAction>

#define OUTPUT_DB INSTALL_PREFIX "/output.sqlite"

MainWindow::MainWindow(QWidget *parent) : KXmlGuiWindow(parent) {
  translator = new Translator("placeholder", "This is a placeholder.", this);
  connect(translator, &Translator::next, this, &MainWindow::NextWord);
  setCentralWidget(translator);
  QAction *openDBAction = new QAction(this);
  openDBAction->setText(i18n("&Open database"));
  actionCollection()->addAction("open", openDBAction);
  actionCollection()->setDefaultShortcut(openDBAction, Qt::CTRL + Qt::Key_O);
  connect(openDBAction, &QAction::triggered, this, &MainWindow::OpenDatabase);
  auto db = QSqlDatabase::addDatabase("QSQLITE", "output_db");
  db.setDatabaseName(OUTPUT_DB);

  KStandardAction::quit(qApp, &QCoreApplication::quit, actionCollection());

  setupGUI(Default, "kindlefisherui.rc");
}

void MainWindow::OpenDatabase() {
  qDebug() << "Works";
  QFileDialog dialog(this);
  dialog.setFileMode(QFileDialog::ExistingFile);
  if (dialog.exec()) {
    db_path = dialog.selectedFiles()[0];
    kindle_db = QSqlDatabase::addDatabase("QSQLITE", "kindle_db");
    kindle_db.setDatabaseName(db_path);

    if (!kindle_db.open()) {
      qDebug() << "Couldn't connect to db";
      return;
    }
    words = QSqlQuery(kindle_db);
    words.prepare("SELECT word, usage FROM [WORDS] LEFT JOIN LOOKUPS ON "
                  "[WORDS].[ID] = [LOOKUPS].[word_key]");
    words.exec();
    NextWord(true);
  }
}

void MainWindow::NextWord(bool skip) {
  if(words.next()) {
    QString word = words.value(0).toString();
    QString example = words.value(1).toString();
    qDebug() << "next";
    layout()->removeWidget(translator);
    delete translator;
    translator = new Translator(word, example, this);
    connect(translator, &Translator::next, this, &MainWindow::NextWord);
    setCentralWidget(translator);
  }
}
