
#include <QApplication>
#include <QCommandLineParser>
#include <KAboutData>
#include <KLocalizedString>
#include "main-window.h"
 
int main (int argc, char *argv[])
{
    QApplication app(argc, argv);
    KLocalizedString::setApplicationDomain("mainwindow");
    
    KAboutData aboutData(
        QStringLiteral("kindlefisher"),
        i18n("kindlefisher"),
        QStringLiteral("1.0"),
        i18n("A simple text area"),
        KAboutLicense::GPL,
        i18n("(c) 2023"),
        i18n("Some text..."),
        QStringLiteral("avalanche.surge.sh"),
        QStringLiteral("antoni.duda@programmer.net"));

    aboutData.addAuthor(i18n("Name"), i18n("Task"),
        QStringLiteral("antoni.duda@programmer.net"),
        QStringLiteral("avalanche.surge.sh"),
        QStringLiteral("OSC Username"));

    KAboutData::setApplicationData(aboutData);
 
    QCommandLineParser parser;
    aboutData.setupCommandLine(&parser);
    parser.process(app);
    aboutData.processCommandLine(&parser);
    
    MainWindow *window = new MainWindow();
    window->show();
    
    return app.exec();
}
