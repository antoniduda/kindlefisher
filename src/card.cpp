#include "card.h"

#include <memory>

#include <QLabel>
#include <QVBoxLayout>
#include <qnamespace.h>

CardBack::CardBack(std::shared_ptr<Word> word, QWidget *parent)
    : QFrame(parent) {
  setFrameStyle(QFrame::StyledPanel | QFrame::Sunken);
  setLineWidth(1);
  this->word = word;

  auto layout = new QVBoxLayout(this);
  setLayout(layout);

  auto word_label = new QLabel(word->word, this);
  word_label->setStyleSheet("font-weight: bold");
  layout->addWidget(word_label, 0, Qt::AlignHCenter);

  auto line = new QFrame(this);
  line->setFrameShape(QFrame::HLine);
  line->setFrameShadow(QFrame::Sunken);
  layout->addWidget(line);

  layout->addWidget(new QLabel("Translations:", this));

  for (const auto &gloss : word->glosses) {
    auto gloss_label = new QLabel(gloss, this);
    gloss_label->setIndent(INDENT);
    gloss_label->setWordWrap(true);
    layout->addWidget(gloss_label, 1);
  }

  layout->addWidget(new QLabel("Example:", this));

  auto example_label = new QLabel(word->example);
  example_label->setIndent(INDENT);
  example_label->setWordWrap(true);
  layout->addWidget(example_label, 1);
}

CardFront::CardFront(std::shared_ptr<Word> word, QWidget *parent)
    : QFrame(parent) {
  setFrameStyle(QFrame::StyledPanel | QFrame::Sunken);
  setLineWidth(1);
  this->word = word;

  auto layout = new QVBoxLayout(this);
  setLayout(layout);

  auto word_label = new QLabel(word->word, this);
  word_label->setStyleSheet("font-weight: bold");
  layout->addWidget(word_label, 1, Qt::AlignHCenter);
}

Card::Card(std::shared_ptr<Word> word, QWidget *parent, bool back_first) : QStackedWidget(parent) {
  this->word = word;
  
  front = new CardFront(word, this);
  auto back_card = new CardBack(word, this);
  back = new QScrollArea(this);
  back->setWidget(back_card);
  back->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
  back->setWidgetResizable(true);
  back->setFrameShape(QFrame::NoFrame);
  addWidget(front);
  addWidget(back);

  if(!back_first) {
    setCurrentWidget(front);
    return;
  }
  is_back = true;
  setCurrentWidget(back);
}

void Card::invert() {
  if(is_back)
    setCurrentWidget(front);
  else
    setCurrentWidget(back);
  is_back = !is_back;
}
