#ifndef TRANSLATOR_H
#define TRANSLATOR_H

#include "card.h"
#include "config.h"

#include <vector>
#include <memory>

#include <QWidget>
#include <QListWidget>
#include <QtSql>
#include <QFuture>
#include <QGridLayout>
#include <QPushButton>


#define DATABASE INSTALL_PREFIX "/share/kindlefisher/dict.sqlite"
#define LIST_ELEM(elem) "<li>" + elem + "</li>"
#define EXAMPLE(example) "</ul>Example:<li>" + example + "</li>"

class Translator : public QWidget {
  Q_OBJECT
public:
  explicit Translator(QString word, QString example, QWidget *parent = nullptr);
  ~Translator();

private:
  Card *preview;
  QString word;
  QString example;
  QListWidget *possible_trans_list;
  QGridLayout *layout;
  QPushButton *revert_btn;
  std::shared_ptr<QThreadPool> db_threadpool;
  std::vector<std::shared_ptr<Word>> translations;
  QFutureWatcher<std::vector<std::shared_ptr<Word>>> translation_watcher;
  QFuture<std::vector<std::shared_ptr<Word>>> translations_future;
  QFuture<std::vector<std::shared_ptr<Word>>> Translate();
  void InitDB();
  void Translated();
  void SwapCard(std::shared_ptr<Word> word);
  void WordSelected(int word_idx);
  void Escape(QString input);
signals:
  void next(bool skip=false);
};
#endif
