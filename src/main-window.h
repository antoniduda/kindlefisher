#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "translator.h"

#include <KXmlGuiWindow>
#include <QSqlDatabase>


class MainWindow : public KXmlGuiWindow {
  Q_OBJECT
public:
  explicit MainWindow(QWidget *parent = nullptr);

private:
  Translator *translator;
  QString db_path;
  QSqlDatabase kindle_db;
  QSqlQuery words;
  void NextWord(bool skip=false);

private Q_SLOTS:
  void OpenDatabase();
};

#endif // MAINWINDOW_H
