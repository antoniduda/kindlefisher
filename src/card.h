#ifndef CARD_H
#define CARD_H

#include <list>
#include <memory>

#include <QFrame>
#include <QStackedWidget>
#include <QScrollArea>

#define INDENT 10

typedef struct Word {
  QString word;
  QString example;
  std::list<QString> glosses;
} Word;

class CardBack : public QFrame {
  Q_OBJECT
public:
  explicit CardBack(std::shared_ptr<Word> word, QWidget *parent = nullptr);

private:
  std::shared_ptr<Word> word;
};

class CardFront : public QFrame {
  Q_OBJECT
public:
  explicit CardFront(std::shared_ptr<Word> word, QWidget *parent = nullptr);

private:
  std::shared_ptr<Word> word;
};
class Card : public QStackedWidget {
  Q_OBJECT
public:
  explicit Card(std::shared_ptr<Word> word, QWidget *parent = nullptr,
                bool back_first = false);
  void invert();
private:
  std::shared_ptr<Word> word;
  bool is_back = false;
  QScrollArea *back;
  CardFront *front;
};
#endif
