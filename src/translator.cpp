#include "translator.h"
#include "card.h"

#include <future>
#include <list>
#include <memory>

#include <QFrame>
#include <QGridLayout>
#include <QLabel>
#include <QListWidget>
#include <QPushButton>
#include <QThreadPool>
#include <QtConcurrent>
#include <QtSql>
#include <qnamespace.h>
#include <QTextStream>

Translator::Translator(QString word, QString example, QWidget *parent)
    : QWidget(parent) {
  this->word = word;
  this->example = example;

  layout = new QGridLayout(this);
  auto word_label = new QLabel(this->word, this);
  word_label->setMaximumHeight(50);
  auto next_button = new QPushButton("Next", this);
  auto skip_button = new QPushButton("Skip", this);

  connect(next_button, &QPushButton::clicked, this, [this]() { 
    auto word = translations[possible_trans_list->currentRow()];
    QString result;
    for(auto &gloss: word->glosses)
      result+=LIST_ELEM(gloss);
    result+=EXAMPLE((*word).example);
    Escape(result);
    result = "\"" + result +  "\";" + word->word;
    QString filename = "Data.txt";
    QFile file(filename);
    if (file.open(QIODevice::Append)) {
        QTextStream stream(&file);
        stream << result << Qt::endl;
    }
    file.close();

    emit next();
  });

  connect(skip_button, &QPushButton::clicked, this,
          [this]() { emit next(true); });

  db_threadpool = std::make_shared<QThreadPool>(this);
  db_threadpool->setMaxThreadCount(1);

  auto placeholder_word = std::make_shared<Word>();
  placeholder_word->word = "Loading...";
  // placeholder_word->glosses.push_back("pizza");

  layout->addWidget(word_label, 0, 0, 1, 4, Qt::AlignHCenter);
  layout->addWidget(next_button, 0, 5, 1, 1);
  layout->addWidget(skip_button, 0, 4, 1, 1);
  possible_trans_list = new QListWidget(this);
  possible_trans_list->setMinimumWidth(550);
  preview = new Card(placeholder_word, this);
  layout->addWidget(possible_trans_list, 1, 0, 2, 4);
  layout->addWidget(preview, 1, 4, 1, 2);
  revert_btn = new QPushButton("Revert", this);
  layout->addWidget(revert_btn, 2, 4, 1, 2);

  connect(possible_trans_list, &QListWidget::currentRowChanged, this,
          &Translator::WordSelected);

  InitDB();

  connect(&translation_watcher,
          &QFutureWatcher<std::vector<std::shared_ptr<Word>>>::finished, this,
          &Translator::Translated);
  translations_future = Translate();
  translation_watcher.setFuture(translations_future);
}

void Translator::Escape(QString input) {
    for(qsizetype idx = input.indexOf("\"", 0); idx > 0; idx = input.indexOf("\"", idx + 2)) {
      input.insert(idx, "\"");
    }
}

Translator::~Translator() {
  QtConcurrent::run(db_threadpool.get(), [](){
    QSqlDatabase::database("dict_db").close();
    QSqlDatabase::removeDatabase("dict_db");
  });
}


void Translator::InitDB() {
  QtConcurrent::run(db_threadpool.get(), []() {
    qDebug() << DATABASE;
    auto dict_db = QSqlDatabase::addDatabase("QSQLITE", "dict_db");
    dict_db.setDatabaseName(DATABASE);

    if (!dict_db.open())
      qDebug() << "Couldn't connect to db";
  });
}

QFuture<std::vector<std::shared_ptr<Word>>> Translator::Translate() {
  return QtConcurrent::run(db_threadpool.get(), [this]() {
    qDebug() << "Translating" << word;
    QSqlQuery query(QSqlDatabase::database("dict_db"));
    query.prepare("SELECT * FROM [words] "
                  "WHERE [word] == (:word) "
                  "AND [lang_code] =='en'");
    query.bindValue(":word", word.toLower());
    std::vector<std::shared_ptr<Word>> ret;

    int IDindex = 0;

    if (query.exec()) {
      while (query.next()) {
        int id = query.value(IDindex).toInt();
        qDebug() << id;
        QSqlQuery glosses(QSqlDatabase::database("dict_db"));
        glosses.prepare(
            "SELECT gloss FROM [glosses] WHERE [word_id] = (:word_id)");
        glosses.bindValue(":word_id", id);
        std::list<QString> glosses_ret;

        if (glosses.exec()) {
          while (glosses.next())
            glosses_ret.push_back(glosses.value(0).toString());
        }
        auto result = std::make_shared<Word>();
        result->word = word;
        result->example = example;
        result->glosses = glosses_ret;
        ret.push_back(result);
      }
    }
    qDebug() << "done";
    return ret;
  });
}

void Translator::Translated() {
  translations = translations_future.result();

  if(translations.empty()) {
    auto word = std::make_shared<Word>();
    word->word = this->word;
    word->example = example;
    word->glosses = std::list<QString>();
    word->glosses.push_back("No translations");
    translations.push_back(word);
  }

  SwapCard(translations.front());
  for (auto &word : translations) {
    QString combined =
        std::accumulate(std::next(word->glosses.begin()), word->glosses.end(),
                        word->glosses.front(), [](QString a, QString b) {
                          return std::move(a) + "/" + b;
                        });
    possible_trans_list->addItem(combined);
  }
  possible_trans_list->setCurrentRow(0);
}

void Translator::SwapCard(std::shared_ptr<Word> word) {
  layout->removeWidget(preview);
  delete preview;
  preview = new Card(word, this, true);
  layout->addWidget(preview, 1, 4, 1, 2);
  connect(revert_btn, &QPushButton::clicked, preview, &Card::invert);
}

void Translator::WordSelected(int word_idx) {
  SwapCard(translations[word_idx]);
}
