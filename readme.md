# Kindlefisher
A program to convert kindle database into csv flashcards which can be imported into anki.

## Compileing and running
Change into clone directory and run
```
cmake -B build/ -DCMAKE_INSTALL_PREFIX={insert install path here} .
cmake --build ./build
cmake --install build/
```
You can ommit `-DCMAKE_INSTALL_PREFIX={}` if you want to install this on your system.
To run compiled binary use:
```
source build/prefix.sh
{prefix_path}/bin/kindlefisher
```

# license
This uses data from wictionary which is under creative commons license: https://creativecommons.org/licenses/by-sa/3.0/
